BASH = $(shell which bash)
SHELL ?= $(BASH)
VERSION ?= $(shell git describe --tags --abbrev=0 2>/dev/null)
NEXT_VERSION ?= $(shell echo ${VERSION} | awk -F. '{$$NF+=1; OFS="."; print $$0}')
LDFLAGS = -ldflags "-s -w -X main.Version=${VERSION} -X main.BuildDate=`date -u +%Y-%m-%d.%H:%M:%S`"
COVERAGE_DIR ?= .coverage
SOURCE_FILES ?= ./...
TEST_PATTERN ?= .
TEST_OPTIONS ?=
APP_NAMESPACE = pm

.PHONY:

build-server:
	CGO_ENABLED=0 go build ${LDFLAGS} -o .build/${APP_NAMESPACE}-server cmd/server/main.go
	@file  .build/${APP_NAMESPACE}-server
	@du -h .build/${APP_NAMESPACE}-server

build-client:
	CGO_ENABLED=0 go build ${LDFLAGS} -o .build/${APP_NAMESPACE}-client cmd/client/main.go
	@file  .build/${APP_NAMESPACE}-client
	@du -h .build/${APP_NAMESPACE}-client

run-server: build-server
	./.build/${APP_NAMESPACE}-server -d

run-client: build-client
	./.build/${APP_NAMESPACE}-client -d

test:
	@mkdir -p $(COVERAGE_DIR)
	@go test $(TEST_OPTIONS) \
		-failfast \
		-race \
		-coverpkg=./... \
		-covermode=atomic \
		-coverprofile=${COVERAGE_DIR}/coverage.txt $(SOURCE_FILES) \
		-run $(TEST_PATTERN) \
		-timeout=2m
