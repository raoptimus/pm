package client

import (
	"fmt"
	"strconv"
)

type parserState int

const (
	OpStart parserState = iota
	Msg
	MsgArg
	MsgPayload
	MsgEndR
	MsgEndN
	//Ping
)

const (
	LenCrLf = 2
)

func (c *Client) parse(buf []byte) error {
	var (
		i         int
		b         byte
		err       error
		as        int
		msgArgCid int64
	)

	for i = 0; i < len(buf); i++ {
		b = buf[i]

		switch c.state {
		case OpStart:
			switch b {
			case 'M', 'm':
				c.state = Msg
				c.header = make([]byte, 0)
				c.msgArgBuf = make([]byte, 0)
			default:
				goto parseErr
			}
		case Msg:
			switch b {
			case '\t', ' ':
				c.msgSize, err = strconv.Atoi(string(c.header))
				if err != nil {
					goto parseErr
				}
				c.state = MsgArg
			default:
				if c.header != nil {
					c.header = append(c.header, b)
				}
			}
		case MsgArg:
			switch b {
			case '\t', ' ':
				msgArgCid, err = strconv.ParseInt(string(c.msgArgBuf), 0, 10)
				if err != nil {
					goto parseErr
				}
				c.msgArgCid = uint64(msgArgCid)
				as = i + 1
				c.state = MsgPayload

				if c.msgBuf == nil {
					lrem := len(buf[as:])
					if lrem > c.msgSize+LenCrLf {
						goto parseErr
					}
					c.msgBuf = make([]byte, lrem, c.msgSize+LenCrLf)
					copy(c.msgBuf, buf[as:])
					i = as + c.msgSize - LenCrLf
				}
			default:
				if c.msgArgBuf != nil {
					c.msgArgBuf = append(c.msgArgBuf, b)
				}
			}
		case MsgPayload:
			if c.msgBuf != nil {
				toCopy := c.msgSize - len(c.msgBuf)
				if toCopy <= 0 {
					c.state = MsgEndR
					continue
				}
				avail := len(buf) - i

				if avail < toCopy {
					toCopy = avail
				}
				if toCopy > 0 {
					start := len(c.msgBuf)
					c.msgBuf = c.msgBuf[:start+toCopy]
					copy(c.msgBuf[start:], buf[i:i+toCopy])
					i = (i + toCopy) - 1
				} else {
					c.msgBuf = append(c.msgBuf, b)
				}
				if len(c.msgBuf) >= c.msgSize {
					c.state = MsgEndR
				}
			} else if i-as+1 >= c.msgSize {
				c.state = MsgEndR
			}

		case MsgEndR:
			if b != '\r' {
				goto parseErr
			}
			if c.msgBuf != nil && len(c.msgBuf) < c.msgSize+LenCrLf {
				c.msgBuf = append(c.msgBuf, b)
			}
			c.state = MsgEndN

		case MsgEndN:
			if b != '\n' {
				goto parseErr
			}
			if c.msgBuf != nil {
				if len(c.msgBuf) < c.msgSize+LenCrLf {
					c.msgBuf = append(c.msgBuf, b)
				}
			} else {
				c.msgBuf = buf[as : i+1]
			}

			c.processInboundMsg(c.msgBuf)
			c.msgBuf, c.header, c.msgArgBuf = nil, nil, nil
			c.msgSize = 0
			c.state = OpStart
			as = i + 1
		}
	}

	return nil

parseErr:
	c.sendErr("Unknown Protocol Operation")
	return fmt.Errorf("parser ERROR, state=%d, i=%d, b=%s, error='%s...'", c.state, i, string(b), err)

}
