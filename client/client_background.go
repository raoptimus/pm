package client

import (
	log "github.com/sirupsen/logrus"
	"time"
)

func (c *Client) readLoop() {
	nc := c.conn
	b := make([]byte, bufSize)

	var _bufs [1][]byte
	bufs := _bufs[:1]

	for {
		var n int
		var err error

		n, err = nc.Read(b)
		if n == 0 && err != nil {
			c.closeConnection(err)
			return
		}

		bufs[0] = b[:n]

		for i := 0; i < len(bufs); i++ {
			if err := c.parse(bufs[i]); err != nil {
				c.closeConnection(err)

				return
			}
		}

		c.last = time.Now().UTC()
	}
}

func (c *Client) processInboundMsg(data []byte) {
	log.Infof("in: data=%s", string(data[:len(data)-LenCrLf]))

	c.inbound <- &message{
		data: data,
	}
}
