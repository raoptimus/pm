package client

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type (
	message struct {
		data []byte
	}
	Options struct {
		ServerAddr string
	}
	Client struct {
		sync.RWMutex
		options   Options
		signal    chan os.Signal
		shutdown  chan struct{}
		done      chan struct{}
		cid       uint64
		start     time.Time
		last      time.Time
		conn      net.Conn
		port      uint16
		host      string
		closed    bool
		err       error
		msgBuf    []byte
		msgSize   int
		msgArgCid uint64
		header    []byte
		msgArgBuf []byte
		state     parserState
		inbound   chan *message
	}
)

const (
	bufSize = 512
)

func New(options Options) (*Client, error) {
	c := &Client{
		signal:   make(chan os.Signal),
		shutdown: make(chan struct{}, 1),
		done:     make(chan struct{}, 1),
		inbound:  make(chan *message, 1024),

		options: options,
	}

	return c, nil
}

func (c *Client) WriteMessage(data []byte, cid uint64) error {
	_, err := c.conn.Write([]byte(fmt.Sprintf("M%d %d %s\r\n", len(data), cid, string(data))))
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) MessagesCount() int {
	return len(c.inbound)
}

func (c *Client) ReadMessage() <-chan *message {
	return c.inbound
}

func (c *Client) Start(waitSignal bool) error {
	if err := c.connect(); err != nil {
		return err
	}
	go c.readLoop()
	log.Info("client started")

	if waitSignal {
		signal.Notify(c.signal, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

		select {
		case sig := <-c.signal:
			log.Infof("client has received shutdown signal [%s]", sig)
			c.done <- struct{}{} //temporary
			c.Stop()
		}
	}

	return nil
}

func (c *Client) connect() error {
	var err error
	log.Infof("connection to server by addr=%s", c.options.ServerAddr)
	c.conn, err = net.Dial("tcp", c.options.ServerAddr)
	if err != nil {
		return fmt.Errorf("dial error: %s", err)
	}

	return err
}

func (c *Client) Stop() {
	log.Info("client shutdown gracefully")
	go c.beforeStop()
	c.shutdown <- struct{}{}

	timeout := time.Tick(10 * time.Second)
	select {
	case <-c.done:
		c.afterStop()
		log.Info("client stopped successfully")
		return
	case <-timeout:
		c.afterStop()
		log.Info("client stop by timeout")
		return
	}
}

func (c *Client) beforeStop() {
	c.done <- struct{}{}
}

func (c *Client) afterStop() {

}

func (c *Client) sendErr(msg string) {
	log.Error(msg)
}

func (c *Client) closeConnection(err error) {
	log.Errorf("close connection: cid=%d, error=%s", c.cid, err)

	c.Lock()
	if c.closed {
		c.Unlock()
		return
	}

	_ = c.conn.Close()
	c.err = err
	c.closed = true
	c.Unlock()
}
