module github.com/raoptimus/pm

go 1.15

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
	github.com/urfave/cli v1.22.5
)
