package server

import (
	log "github.com/sirupsen/logrus"
	"net"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type (
	client struct {
		sync.RWMutex
		cid       uint64
		srv       *Server
		start     time.Time
		last      time.Time
		conn      net.Conn
		port      uint16
		host      string
		closed    bool
		err       error
		msgBuf    []byte
		msgSize   int
		msgArgCid uint64
		header    []byte
		msgArgBuf []byte
		state     parserState
		outbound  chan *message
	}
)

func (c *client) initClient() {
	s := c.srv
	c.cid = atomic.AddUint64(&s.gcid, 1)

	var conn string
	if c.conn != nil {
		if addr := c.conn.RemoteAddr(); addr != nil {
			if conn = addr.String(); conn != "" {
				host, port, _ := net.SplitHostPort(conn)
				iPort, _ := strconv.Atoi(port)
				c.host, c.port = host, uint16(iPort)
				conn = strings.ReplaceAll(conn, "%", "%%")
			}
		}
	}

	//todo set ping timer

	log.Debugf("%s - cid:%d", conn, c.cid)
}

func (c *client) sendErr(msg string) {
	log.Debug(msg)
}

func (c *client) processInboundMsg(msgBuf []byte, cid uint64) {
	log.Infof("in: data=%s, cid=%d", string(msgBuf[:len(msgBuf)-LenCrLf]), cid)

	c.srv.messages <- &message{
		cid:  cid,
		data: msgBuf,
	}
}

func (c *client) closeConnection(err error) {
	log.Debugf("close connection: cid=%d, error=%s", c.cid, err)

	c.Lock()
	if c.closed {
		c.Unlock()
		return
	}

	_ = c.conn.Close()
	c.err = err
	c.closed = true
	c.Unlock()

	c.srv.removeClient(c)
}
