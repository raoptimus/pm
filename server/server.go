package server

import (
	log "github.com/sirupsen/logrus"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type (
	Options struct {
		ServerAddr string
		MaxClients int
	}
	Server struct {
		sync.RWMutex

		opt Options

		signal           chan os.Signal
		shutdown         bool
		shutdownComplete chan struct{}
		done             chan struct{}
		listener         net.Listener
		gcid             uint64
		totalClients     int
		clients          map[uint64]*client
		messages         chan *message
	}
	message struct {
		cid  uint64
		data []byte
	}
)

const (
	channelSizeWithMessages = 1024
)

func New(opt Options) *Server {
	return &Server{
		opt:              opt,
		signal:           make(chan os.Signal),
		shutdown:         false,
		shutdownComplete: make(chan struct{}),
		done:             make(chan struct{}),
		clients:          map[uint64]*client{},
		messages:         make(chan *message, channelSizeWithMessages),
	}
}

func (s *Server) Start(waitSignal bool) error {
	go s.acceptLoop()
	go s.exchangeLoop()

	if waitSignal {
		signal.Notify(s.signal, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

		select {
		case sig := <-s.signal:
			log.Infof("service has received shutdown signal [%s]", sig)
			s.done <- struct{}{} //temporary
			s.Stop()
		}
	}

	return nil
}

func (s *Server) Stop() {
	log.Info("service shutdown gracefully")
	go s.beforeStop()

	timeout := time.Tick(10 * time.Second)
	select {
	case <-s.done:
		s.afterStop()
		log.Info("service stopped successfully")
		return
	case <-timeout:
		s.afterStop()
		log.Info("service stop by timeout")
		return
	}
}

func (s *Server) beforeStop() {
	s.Shutdown()
	s.WaitForShutdown()
	s.done <- struct{}{}
}

func (s *Server) afterStop() {

}

func (s *Server) Shutdown() {
	if s == nil {
		return
	}

	s.Lock()
	if s.shutdown {
		s.Unlock()
		return
	}

	s.shutdown = true
	s.Unlock()

	s.Lock()
	defer s.Unlock()

	for _, c := range s.clients {
		_ = c.conn.Close()
	}

	if s.listener != nil {
		s.listener.Close()
	}

	// Notify that the shutdown is complete
	close(s.shutdownComplete)
}

func (s *Server) WaitForShutdown() {
	<-s.shutdownComplete
}

func (s *Server) removeClient(c *client) {
	log.Debugf("remove client, cid=%d", c.cid)

	s.Lock()
	delete(s.clients, c.cid)
	s.totalClients--
	s.Unlock()
}
