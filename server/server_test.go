package server

import (
	"fmt"
	conn "github.com/raoptimus/pm/client"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var srv *Server

const srvAddr = "localhost:8081"

func serverInit(t *testing.T) {
	if srv != nil {
		return
	}
	srv = New(Options{
		ServerAddr: srvAddr,
		MaxClients: 0, //no limits
	})
	err := srv.Start(false)
	assert.NoError(t, err)
}

func TestSendMessageToSpecClient(t *testing.T) {
	serverInit(t)

	if !assert.NotNil(t, srv) {
		return
	}
	var (
		err error
		cl  *conn.Client
	)

	cls := make([]*conn.Client, 10)
	expectedMessagesCount := make([]int, 10)
	for i := 0; i < 10; i++ {
		cl, err = conn.New(conn.Options{
			ServerAddr: srvAddr,
		})
		assert.NoError(t, err)
		err = cl.Start(false)
		assert.NoError(t, err)
		cls[i] = cl
		expectedMessagesCount[i] = 0
	}

	expectedMessagesCount[1] = 1

	//write message to client with cid=2
	msg := "test message"
	err = cls[0].WriteMessage([]byte(msg), 2)
	assert.NoError(t, err)

	//waiting for message
	time.Sleep(1 * time.Second)

	//check messages in clients
	for i, expCount := range expectedMessagesCount {
		assert.Equal(t, expCount, cls[i].MessagesCount(), fmt.Sprintf("#%d", i))
	}
}

func TestSendMessageToAllClients(t *testing.T) {
	serverInit(t)

	if !assert.NotNil(t, srv) {
		return
	}
	var (
		err error
		cl  *conn.Client
	)

	cls := make([]*conn.Client, 10)
	expectedMessagesCount := make([]int, 10)
	for i := 0; i < 10; i++ {
		cl, err = conn.New(conn.Options{
			ServerAddr: srvAddr,
		})
		assert.NoError(t, err)
		err = cl.Start(false)
		assert.NoError(t, err)
		cls[i] = cl
		expectedMessagesCount[i] = 1
	}

	//write message to all clients
	msg := "test message"
	err = cls[0].WriteMessage([]byte(msg), 0)
	assert.NoError(t, err)

	//waiting for message
	time.Sleep(1 * time.Second)

	//check messages in clients
	for i, expCount := range expectedMessagesCount {
		assert.Equal(t, expCount, cls[i].MessagesCount(), fmt.Sprintf("#%d", i))
	}
}
