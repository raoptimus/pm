package server

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"net"
	"time"
)

func (s *Server) acceptLoop() {
	s.Lock()

	l, err := net.Listen("tcp", s.opt.ServerAddr)
	if err != nil {
		s.Unlock()
		log.Fatalf("Error listening on port: %s, %q", s.opt.ServerAddr, err)
		return
	}
	defer l.Close()
	s.listener = l
	s.Unlock()

	log.Infof("server has started by address=%s", s.opt.ServerAddr)

	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}

		go s.createClient(conn)
	}
}

func (s *Server) createClient(conn net.Conn) {
	now := time.Now().UTC()
	c := &client{
		srv:      s,
		conn:     conn,
		start:    now,
		last:     now,
		outbound: make(chan *message, channelSizeWithMessages),
	}
	c.initClient()

	//todo check max clients
	s.Lock()
	s.totalClients++
	s.clients[c.cid] = c
	s.Unlock()

	go c.readLoop()
	go c.writeLoop()
}

func (s *Server) exchangeLoop() {
	for m := range s.messages {
		if m.cid > 0 {
			s.RLock()
			if c, ok := s.clients[m.cid]; ok {
				s.RUnlock()
				c.outbound <- m
			} else {
				s.RUnlock()
			}
			continue
		}
		s.RLock()
		for _, c := range s.clients {
			c.outbound <- m
		}
		s.RUnlock()
	}
}
