package server

import (
	"fmt"
	"time"
)

const (
	bufSize = 512
)

func (c *client) readLoop() {
	nc := c.conn
	b := make([]byte, bufSize)

	var _bufs [1][]byte
	bufs := _bufs[:1]

	for {
		var n int
		var err error

		n, err = nc.Read(b)
		if n == 0 && err != nil {
			c.closeConnection(err)
			return
		}

		bufs[0] = b[:n]

		for i := 0; i < len(bufs); i++ {
			if err := c.parse(bufs[i]); err != nil {
				c.closeConnection(err)

				return
			}
		}

		c.last = time.Now().UTC()
	}
}

func (c *client) writeLoop() {
	defer close(c.outbound)

	for {
		select {
		case m := <-c.outbound:
			_, err := c.conn.Write([]byte(fmt.Sprintf("M%d %d %s", len(m.data)-LenCrLf, m.cid, string(m.data))))
			if err != nil {
				c.closeConnection(err)
				return
			}
		}
	}
}
