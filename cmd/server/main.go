package main

import (
	"fmt"
	"github.com/raoptimus/pm/server"
	"net/http"
	_ "net/http/pprof"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var (
	BuildDate string
	Version   string
)

func main() {
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("version=%s date=(%s UTC)\n", c.App.Version, BuildDate)
	}

	app := cli.NewApp()
	app.Name = "server"
	app.EnableBashCompletion = true
	app.Usage = "server"
	app.Version = Version

	flags := []cli.Flag{
		cli.BoolFlag{
			Name:   "debug, d",
			EnvVar: "DEBUG",
			Usage:  "Enable debug mode.",
		},
		cli.StringFlag{
			Name:   "debug-addr, da",
			EnvVar: "DEBUG_API_ADDR",
			Value:  "localhost:6061",
			Usage:  "HTTP Server for pprof",
		},
		cli.StringFlag{
			Name:   "server-addr",
			EnvVar: "SERVER_ADDR",
			Value:  "localhost:8080",
			Usage:  "Address for server listening",
		},
		cli.IntFlag{
			Name:   "max-clients",
			EnvVar: "MAX_CLIENTS",
			Value:  100,
			Usage:  "Maximum clients",
		},
	}
	app.Flags = flags

	app.Action = func(ctx *cli.Context) error {
		switch {
		case ctx.Bool("debug"):
			log.SetLevel(log.DebugLevel)
			go func() {
				log.Println(http.ListenAndServe(ctx.String("debug-addr"), nil))
			}()
		default:
			if level, err := log.ParseLevel(ctx.String("log-level")); err == nil {
				log.SetLevel(level)
			}
		}

		options := server.Options{
			ServerAddr: ctx.String("server-addr"),
			MaxClients: ctx.Int("max-clients"),
		}
		serv := server.New(options)

		return serv.Start(true)
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
